<?php


namespace app\controllers;

use yii\web\Controller;
use app\models\Address;
use Yii;

class AddressController extends Controller
{
    public function actionAdd($userId = null)
    {
        $model = new Address();
        if ($model->load(Yii::$app->request->post()) && $userId = Yii::$app->request->post('userId')) {
            if ($model->saveWith($userId)) {
                return $this->goHome();
            }
        }
        return $this->render('add', ['model' => $model, 'userId' => $userId]);
    }

    public function actionUpdate($id)
    {
        $model = Address::findOne(['id' => $id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->goBack();
        } else {
            return $this->render('update', ['model' => $model]);
        }
    }

    public function actionDelete($id)
    {
        $model = Address::findOne($id);

        if (Yii::$app->request->isPost) {
            if ($model->delete()) {
                Yii::$app->session->setFlash('success', 'Address deleted successfully');
            } else {
                Yii::$app->session->setFlash('error', 'Address can\'t be deleted');
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }
}

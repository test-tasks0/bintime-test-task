<?php


namespace app\controllers;

use app\models\SignupForm;
use app\models\Address;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use Yii;
use yii\filters\AccessControl;

class UserController extends Controller
{
    /* public function behaviors()
     {
         return [
             'access' => [
                 'class' => AccessControl::className(),
                 'rules' => [
                     [
                         'allow' => true,
                         'actions' => ['update','delete'],
                         'roles' => ['@'],
                     ],
                 ],
             ],
         ];
     }*/

    public function actionAdd()
    {
        $model = new SignupForm();
        $address = new Address();
        if ($model->load(Yii::$app->request->post()) && $address->load(Yii::$app->request->post())) {
            if ($user = $model->signup($address)) {
                return $this->goHome();
            }
        }
        return $this->render('add', [
            'signupForm' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = User::findOne(['id' => $id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->goHome();
        } else {
            return $this->render('update', ['model' => $model]);
        }
    }

    public function actionDelete($id)
    {
        $model = User::findOne($id);

        if (Yii::$app->request->isPost) {
            $model->unlinkAll('address', true);
            $model->delete();
            Yii::$app->session->setFlash('success', 'User deleted successfully');
        }

        return $this->goHome();
    }

    public function actionView($id)
    {
        $model = User::findOne(['id' => $id]);

        $addressProvider = new ActiveDataProvider([
            'query' => $model->getAddress(),
            'pagination' => [
                'pageSize' => 5
            ]
        ]);

        return $this->render('view', ['user' => $model, 'addressProvider' => $addressProvider]);
    }

    public function actionList()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find()
        ]);

        return $this->render('list', [
            'dataProvider' => $dataProvider
        ]);
    }
}

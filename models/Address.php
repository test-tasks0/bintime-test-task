<?php


namespace app\models;

use yii\db\ActiveRecord;

/**
 * Address model
 *
 * @property integer $id
 * @property string $post_index
 * @property string $country
 * @property string $city
 * @property string $street
 * @property string $house
 * @property string $apartment
 */
class Address extends ActiveRecord
{
    public function rules()
    {
        return [
            [['post_index', 'country', 'city', 'street', 'house'],'required'],
            [['country'], 'match', 'pattern' => '/^[A-Z]{2}$/'],
            ['post_index', 'match', 'pattern' => '/^\d+$/'],
            ['apartment', 'safe']
        ];
    }

    /**
     * Save and link model
     * @return boolean
     */
    public function saveWith($userId)
    {
        $user = User::findOne($userId);
        if (!is_null($user) && $this->save()) {
            $user->link('address', $this);
            return true;
        }
        return false;
    }

    public function getUser()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])
            ->viaTable('user_address', ['address_id' => 'id']);
    }

    public function delete()
    {
        if ((int) $this->getUser()->count() === 1) {
            return false;
        }
        $this->unlinkAll('user', true);
        return parent::delete();
    }

    public function beforeDelete()
    {
        if ((int) $this->getUser()->count() === 1) {
            return false;
        }

        return parent::beforeDelete();
    }
}

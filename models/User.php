<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\behaviors\DateBehavior;
use Carbon\Carbon;
use DateTime;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $email
 * @property string $auth_key
 * @property string $name
 * @property string $surname
 * @property integer $gender
 * @property integer $status
 * @property string $created_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const GENDER_NOINFO = 0;
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    public static $genderLabels = [
        'No info',
        'Male',
        'Female'
    ];

    public function behaviors()
    {
        return [

            'timestamp' => [

                'class' => DateBehavior::className(),

                'attributes' => [

                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', false],

                    ActiveRecord::EVENT_BEFORE_UPDATE => false,

                ],

            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    public function getTime()
    {
        return (Carbon::createFromFormat(DateTime::ISO8601, $this->created_at)->format('d-m-Y h:i'));
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username','name','surname','gender','email'],'required'],
            ['created_at','safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }
    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }
    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }
    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }
    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getPassword()
    {
        return '';
    }

    public function getAddress()
    {
        return $this->hasMany(Address::className(), ['id' => 'address_id'])
            ->viaTable('user_address', ['user_id' => 'id']);
    }
}

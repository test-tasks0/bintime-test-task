<?php


namespace app\models;

use yii\base\Model;

class SignupForm extends Model
{
    public $username;
    public $name;
    public $surname;
    public $gender;
    public $email;
    public $password;

    public function rules()
    {
        return [
            [['username','password','name','surname','gender','email'],'required'],
            ['email', 'email'],
            [
                'username',
                'unique',
                'targetClass' => 'app\models\User',
                'message' => 'This username has already been taken.'
            ],
            [
                'email',
                'unique',
                'targetClass' => 'app\models\User',
                'message' => 'This email address has already been taken.'
            ],
            ['username', 'string', 'min' => 4],
            ['password', 'string', 'min' => 6],
            [['name','surname'], 'match', 'pattern' => '/^[A-ZА-Я][a-zа-я]+$/'],
            ['gender', 'in', 'range' => [User::GENDER_NOINFO, User::GENDER_MALE, User::GENDER_FEMALE]],
        ];
    }

    /**
     * @return User|null
     */
    public function signup(Address $address)
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->name = $this->name;
            $user->surname = $this->surname;
            $user->gender = $this->gender;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();

            if ($user->save() && $address->save()) {
                $user->link('address', $address);
            } else {
                return null;
            }

            return $user;
        }

        return null;
    }

    public function loadAddress()
    {
        //$this->populateRelation
        //if (!$this->isRelationPopulated('cover') && !$this->getCover()->one()) {
        //            $this->setCover(reset($images));
        //        }
    }
}

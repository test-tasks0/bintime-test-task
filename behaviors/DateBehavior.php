<?php

namespace app\behaviors;

use yii\db\BaseActiveRecord;
use yii\behaviors\TimestampBehavior;
use Carbon\Carbon;

class DateBehavior extends TimestampBehavior
{
    public function init()
    {
        parent::init();
        if (empty($this->attributes)) {
            $this->attributes = [
                BaseActiveRecord::EVENT_BEFORE_INSERT => [
                    $this->createdAtAttribute->toIso8601String(),
                    $this->updatedAtAttribute->toIso8601String()
                ],
                BaseActiveRecord::EVENT_BEFORE_UPDATE => $this->updatedAtAttribute->toIso8601String(),
            ];
        }
    }

    protected function getValue($event)
    {
        if ($this->value === null) {
            return Carbon::now()->toIso8601String();
        }
        return parent::getValue($event);
    }
}

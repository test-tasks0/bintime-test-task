<?php
use app\models\User;
use yii\grid\GridView;

$columns = [
    'username',
    'email',
    'name',
    'surname',
    [
        'attribute' => 'gender',
        'value' => function ($model) {
            return User::$genderLabels[$model->gender];
        }
    ],
    'time',
    [
        'class' => 'yii\grid\ActionColumn',
        'template' =>  Yii::$app->user->isGuest ? '{view}' : '{update} {delete}',
    ]
];

?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columns
]);


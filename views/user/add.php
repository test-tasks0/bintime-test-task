
<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use app\models\Address;
use Yii;

$form = ActiveForm::begin(['method' => 'post']);
?>
<?= Yii::$app->controller->renderPartial('_userForm', ['user' => $signupForm, 'form' => $form]) ?>

<?=
$this->renderFile(
    __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'address'.DIRECTORY_SEPARATOR.'_addressForm.php',
    ['address' => new Address(), 'form' => $form]
)
?>

<div class="form-group">
    <?= Html::submitButton('Add user', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
</div>


<?php
ActiveForm::end();
?>

<?php
use app\models\User;

?>

<?= $form->field($user, 'username')->textInput() ?>
<?= $form->field($user, 'name')->textInput() ?>
<?= $form->field($user, 'surname')->textInput() ?>
<?= $form->field($user, 'password')->passwordInput() ?>
<?= $form->field($user, 'gender')->dropDownList(
    [User::GENDER_NOINFO => 'No info', User::GENDER_MALE => 'Male', User::GENDER_FEMALE => 'Female']
); ?>
<?= $form->field($user, 'email')->input('email');

<?php
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\models\User;
use yii\bootstrap\Html;
?>

<?= DetailView::widget([
    'model' => $user,
    'attributes' => [
        'username',
        'email',
        'name',
        'surname',
        [
            'attribute' => 'gender',
            'value' => function ($model) {
                return User::$genderLabels[$model->gender];
            }
        ],
        'time'
    ]
]); ?>

<br/>
<?= Html::a('Create address', ['address/add','userId' => $user->id], ['class' => 'btn btn-success']) ?>
<hr/>

<?= GridView::widget([
    'dataProvider' => $addressProvider,
    'columns' => [
        'post_index',
        'country',
        'city',
        'street',
        'house',
        'apartment',
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'address',
            'template' =>  '{update} {delete}',
        ]
    ]
]); ?>

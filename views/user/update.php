
<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use app\models\Address;
use Yii;

$form = ActiveForm::begin(['method' => 'post']);
?>

<?= Yii::$app->controller->renderPartial('_userForm', ['user' => $model, 'form' => $form]) ?>

<div class="form-group">
    <?= Html::submitButton('Update user', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
</div>


<?php
ActiveForm::end();
?>

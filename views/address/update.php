<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$form = ActiveForm::begin(['method' => 'post']);

?>

<?= Yii::$app->controller->renderPartial('_addressForm', ['address' => $model, 'form' => $form]) ?>

<div class="form-group">
    <?= Html::submitButton('Update address', ['class' => 'btn btn-primary']) ?>
</div>

<?php
ActiveForm::end();
?>

<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$form = ActiveForm::begin(['method' => 'post']);

?>

<?= Yii::$app->controller->renderPartial('_addressForm', ['address' => $model, 'form' => $form]) ?>

<?= Html::hiddenInput('userId', $userId); ?>

<div class="form-group">
    <?= Html::submitButton('Add address', ['class' => 'btn btn-primary']) ?>
</div>

<?php
ActiveForm::end();
?>

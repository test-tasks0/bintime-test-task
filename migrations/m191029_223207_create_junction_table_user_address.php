<?php

use yii\db\Migration;

/**
 * Class m191029_223207_create_junction_table_user_address
 */
class m191029_223207_create_junction_table_user_address extends Migration
{
    public function up()
    {
        $this->createTable('{{%user_address}}', [
            'id' => $this->primaryKey(),
            'address_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(
            'fk-address_id-id',
            '{{%user_address}}',
            'address_id',
            '{{%address}}',
            'id'
        );

        $this->addForeignKey(
            'fk-user_id-id',
            '{{%user_address}}',
            'user_id',
            '{{%user}}',
            'id'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk-address_id-id', '{{%user_address}}');
        $this->dropForeignKey('fk-user_id-id','{{%user_address}}');
        $this->dropTable('{{%user_address}}');
    }
}

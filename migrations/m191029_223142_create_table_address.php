<?php

use yii\db\Migration;

/**
 * Class m191029_223142_create_table_address
 */
class m191029_223142_create_table_address extends Migration
{
    public function up()
    {
        $this->createTable('{{%address}}', [
            'id' => $this->primaryKey(),
            'post_index' => $this->string()->notNull(),
            'country' => $this->string(2)->notNull(),
            'city' => $this->string()->notNull(),
            'street' => $this->string()->notNull(),
            'house' => $this->string()->notNull(),
            'apartment' => $this->string()->notNull()
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%address}}');
    }
}
